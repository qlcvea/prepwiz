package com.qlcvea.prepwiz.async;

public interface ProgressCallback {
    void onProgressChanged(Progress newProgress, SingleInstanceProgressAsyncOperation operation);
}
