package com.qlcvea.prepwiz.async;

public class Progress {
    public boolean indeterminate;
    public String status;
    public final int progressMax;
    public final int progressMin;
    public final int progressValue;

    /**
     * Create a new {@link Progress} with a determinate progress value.
     * @param status Status text.
     * @param progressValue Current progress value. Must be >= {@link #progressMin} and <= {@link #progressMax}.
     * @param progressMax Maximum progress value. Must be > {@link #progressMin}.
     * @param progressMin Minimum progress value. Must be ><{@link #progressMax}.
     */
    public Progress(String status, int progressValue, int progressMax, int progressMin) {
        if (progressMin >= progressMax) throw new ArithmeticException("Minimum value cannot be greater or equal to maximum value");
        if (progressValue > progressMax) throw new ArithmeticException("Progress value must be less than or equal to maximum value");
        if (progressValue < progressMin) throw new ArithmeticException("Progress value must be greater than or equal to minimum value");
        this.status = status;
        this.indeterminate = false;
        this.progressMax = progressMax;
        this.progressMin = progressMin;
        this.progressValue = progressValue;
    }

    /**
     * Create a new {@link Progress} with an indeterminate progress value.
     * @param status Status text.
     */
    public Progress(String status) {
        this.status = status;
        this.indeterminate = true;
        this.progressMax = 0;
        this.progressMin = 0;
        this.progressValue = 0;
    }
}
