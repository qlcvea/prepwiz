package com.qlcvea.prepwiz.async;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public abstract class SingleInstanceProgressAsyncOperation<T> {
    private CompletableFuture<T> currentOperation = null;

    private Progress currentProgress = null;

    private List<ProgressCallback> progressCallbacks = new ArrayList<>(1);

    /**
     * Add a {@link ProgressCallback} that will be triggered when the value of {@link #getProgress()} changes.
     * @param callback A {@link ProgressCallback} that will be triggered when the value of {@link #getProgress()} changes.
     */
    public void addProgressCallback(ProgressCallback callback) {
        if (!progressCallbacks.contains(callback)) progressCallbacks.add(callback);
    }

    /**
     * Remove a {@link ProgressCallback} from the list of callbacks that will be triggered when the value of {@link #getProgress()} changes.
     * @param callback A {@link ProgressCallback} that will not be triggered when the value of {@link #getProgress()} changes.
     */
    public void removeProgressCallback(ProgressCallback callback) {
        if (progressCallbacks.contains(callback)) progressCallbacks.remove(callback);
    }

    /**
     * Change the current progress information and run all progress callbacks.
     */
    protected void setProgress(Progress progress) {
        currentProgress = progress;
        for (ProgressCallback callback : progressCallbacks) {
            callback.onProgressChanged(progress, this);
        }
    }

    /**
     * Get the current progress information.
     * @return The current progress information or {@code null}.
     */
    public Progress getProgress() {
        return currentProgress;
    }

    /**
     * Get the current operation as a {@link CompletableFuture}.
     * @return The current operation as a {@link CompletableFuture} or {@code null}.
     */
    public CompletableFuture<T> getCurrentOperation() {
        return currentOperation;
    }

    /**
     * Check if the operation is running.
     * @return {@code true} if the operation is running, {@code false} otherwise.
     */
    public boolean isRunning() {
        return currentOperation != null && !currentOperation.isCancelled() && !currentOperation.isDone();
    }

    /**
     * Throw an exception if the operation is running.
     * Use this in setters in child classes to ensure values do not get changed while your operation is running.
     * @throws IllegalStateException if the operation is running.
     */
    protected void failIfRunning() {
        if (isRunning()) throw new IllegalStateException("Operation is running.");
    }

    /**
     * Start the operation.
     * @return A {@link CompletableFuture} that will complete once the operation is completed.
     * @throws IllegalStateException if the operation is running or if {@link #isReady()} returns {@code false}.
     */
    public CompletableFuture<T> start() {
        if (isRunning()) {
            throw new IllegalStateException("Operation is already running");
        }

        if (!isReady()) {
            throw new IllegalStateException("Not ready");
        }

        currentProgress = null;
        currentOperation = CompletableFuture.supplyAsync(this::main);
        return currentOperation;
    }

    /**
     * Check if the operation can begin.
     * @return {@code true} if the operation can begin, {@code false} otherwise.
     */
    protected abstract boolean isReady();

    /**
     * The main operation.
     * This method will be executed when {@link #start()} is called, but only if {@link #isReady()} returns {@code true}.
     */
    protected abstract T main();
}
