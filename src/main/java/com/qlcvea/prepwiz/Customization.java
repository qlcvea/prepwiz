package com.qlcvea.prepwiz;

import org.apache.commons.cli.*;

import java.io.IOException;
import java.util.Properties;

public class Customization {
    public static Customization INSTANCE = new Customization();

    private String packUrl = null;
    private String title = "prepwiz";
    private boolean automated = false;
    private String defaultInstallPath = null;
    private String defaultInstanceName = null;
    private boolean defaultUseIcon = false;
    private boolean defaultUseJoinSetting = false;
    private boolean defaultUseMemorySetting = false;
    private boolean doOutput = false;

    public Customization() {
        // attempt to read customizations from custom.properties resource file
        Properties customProperties = new Properties();
        try {
            customProperties.load(this.getClass().getClassLoader().getResourceAsStream("custom.properties"));
        } catch (IOException ignored) {
        } // fail silently, not critical to operation
        if (customProperties.containsKey("packUrl")) {
            // check to ensure it is not blank or null because the field will be disabled
            String packUrl = customProperties.getProperty("packUrl");
            if (packUrl != null && !packUrl.trim().isEmpty()) {
                this.packUrl = packUrl;
            }
        }
        if (customProperties.containsKey("title")) {
            // check to ensure it is not blank or null
            String title = customProperties.getProperty("title");
            if (title != null && !title.trim().isEmpty()) {
                this.title = title;
            }
        }
        if (customProperties.containsKey("automated")) {
            automated = customProperties.getProperty("automated").equalsIgnoreCase("true");
        }
        if (customProperties.containsKey("defaultInstallPath")) {
            defaultInstallPath = customProperties.getProperty("defaultInstallPath");
        }
        if (customProperties.containsKey("defaultInstanceName")) {
            defaultInstanceName = customProperties.getProperty("defaultInstanceName");
        }
        if (customProperties.containsKey("defaultUseIcon")) {
            defaultUseIcon = customProperties.getProperty("defaultUseIcon").equalsIgnoreCase("true");
        }
        if (customProperties.containsKey("defaultUseJoinSetting")) {
            defaultUseJoinSetting = customProperties.getProperty("defaultUseJoinSetting").equalsIgnoreCase("true");
        }
        if (customProperties.containsKey("defaultUseMemorySetting")) {
            defaultUseMemorySetting = customProperties.getProperty("defaultUseMemorySetting").equalsIgnoreCase("true");
        }
    }

    /**
     * Parse options into this object from command line arguments.
     * @param args Command line arguments
     * @param allowHelp Allow the usage of a "-h" flag to show help text.
     * @return {@code true} if help was NOT activated, {@code false} if it was. Application should exit if this method returns {@code false}.
     */
    public boolean parseArgs(String[] args, boolean allowHelp) throws ParseException {
        Options options = new Options();
        if (allowHelp) options.addOption("h", "help", false, "Display help");
        options.addOption(null, "title", true, "Window title");
        options.addOption(null, "pack-url", true, "Pack URL (not editable if set)");
        options.addOption(null, "default-install-path", true, "Default installation path");
        options.addOption(null, "default-instance-name", true, "Default instance name");
        options.addOption(null, "use-icon-default-enabled", false, "Specify this option to enable the Download icon checkbox by default");
        options.addOption(null, "use-join-setting-default-enabled", false, "Specify this option to enable the Automatically connect to server checkbox by default");
        options.addOption(null, "use-memory-setting-default-enabled", false, "Specify this option to enable the Apply recommended memory settings checkbox by default");
        options.addOption("o", "output", false, "Output installation information when done");
        options.addOption("a", "automated", false, "Automatically begin installation as soon as the window opens");
        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = parser.parse(options, args);
        if (allowHelp && cmd.hasOption("h")) {
            HelpFormatter help = new HelpFormatter();
            help.printHelp("java -jar prepwiz.jar", options);
            return false;
        }
        if (cmd.hasOption("pack-url")) {
            // check to ensure it is not blank or null because the field will be disabled
            String packUrl = cmd.getOptionValue("pack-url");
            if (packUrl != null && !packUrl.trim().isEmpty()) {
                this.packUrl = packUrl;
            }
        }
        if (cmd.hasOption("title")) {
            // check to ensure it is not blank or null
            String title = cmd.getOptionValue("title");
            if (title != null && !title.trim().isEmpty()) {
                this.title = title;
            }
        }
        if (cmd.hasOption("a")) automated = true;
        if (cmd.hasOption("default-install-path")) {
            defaultInstallPath = cmd.getOptionValue("default-install-path");
        }
        if (cmd.hasOption("default-instance-name")) {
            defaultInstanceName = cmd.getOptionValue("default-instance-name");
        }
        if (cmd.hasOption("use-icon-default-enabled")) defaultUseIcon = true;
        if (cmd.hasOption("use-join-setting-default-enabled")) defaultUseJoinSetting = true;
        if (cmd.hasOption("use-memory-setting-default-enabled")) defaultUseMemorySetting = true;
        doOutput = cmd.hasOption("o");
        return true;
    }

    public String getTitle() {
        return title;
    }

    public String getPackUrl() {
        return packUrl;
    }

    public boolean getAutomated() {
        return automated;
    }

    public String getDefaultInstallPath() {
        return defaultInstallPath;
    }

    public String getDefaultInstanceName() {
        return defaultInstanceName;
    }

    public boolean getDefaultUseIcon() {
        return defaultUseIcon;
    }

    public boolean getDefaultUseJoinSetting() {
        return defaultUseJoinSetting;
    }

    public boolean getDefaultUseMemorySetting() {
        return defaultUseMemorySetting;
    }

    public boolean getDoOutput() {
        return doOutput;
    }
}
