package com.qlcvea.prepwiz;

import com.google.gson.Gson;
import com.qlcvea.prepwiz.platform.Platform;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.ResourceBundle;

public class MainDialog extends JDialog {
    private JPanel contentPane;
    private JTextField textFieldPackUrl;
    private JLabel labelPackUrl;
    private JLabel labelInstallPath;
    private JTextField textFieldInstallPath;
    private JButton buttonBrowseInstallPath;
    private JCheckBox checkBoxUseMemorySetting;
    private JTextField textFieldInstanceName;
    private JLabel labelInstanceName;
    private JCheckBox checkBoxUseJoinSetting;
    private JButton buttonInstall;
    private JCheckBox checkBoxUseIcon;

    private final ResourceBundle strings = ResourceBundle.getBundle("lang/strings");

    public MainDialog() {
        // this makes the icon show up in the taskbar
        // https://stackoverflow.com/a/25533860/10840332
        super((Dialog) null);

        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonInstall);

        buttonBrowseInstallPath.addActionListener(e -> onBrowse());

        buttonInstall.addActionListener(e -> onInstall());

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(e -> onCancel(), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    public static void main(String[] args) {
        try {
            if (!Customization.INSTANCE.parseArgs(args, true)) {
                System.exit(0);
                return;
            }
        } catch (Exception ex) {
            System.err.println("Failed to parse command line options: " + ex.getMessage());
            ex.printStackTrace(System.err);
            System.exit(1);
            return;
        }

        MainDialog dialog = new MainDialog();

        dialog.setTitle(Customization.INSTANCE.getTitle());
        if (Customization.INSTANCE.getPackUrl() != null) {
            dialog.textFieldPackUrl.setText(Customization.INSTANCE.getPackUrl());
            dialog.textFieldPackUrl.setEnabled(false);
        }
        dialog.textFieldInstanceName.setText(Customization.INSTANCE.getDefaultInstanceName());
        dialog.checkBoxUseIcon.setSelected(Customization.INSTANCE.getDefaultUseIcon());
        dialog.checkBoxUseJoinSetting.setSelected(Customization.INSTANCE.getDefaultUseJoinSetting());
        dialog.checkBoxUseMemorySetting.setSelected(Customization.INSTANCE.getDefaultUseMemorySetting());

        // check if platform is supported
        if (Platform.getCurrentPlatform() == null) {
            JOptionPane.showMessageDialog(dialog,
                    dialog.strings.getString("error.unsupportedPlatform"),
                    dialog.strings.getString("error"),
                    JOptionPane.ERROR_MESSAGE);
            dialog.dispose();
            System.exit(1);
            return;
        }

        // attempt to find launcher installation
        String defaultInstallPath = Customization.INSTANCE.getDefaultInstallPath();
        if (defaultInstallPath == null) {
            String launcherInstallPath;
            launcherInstallPath = Platform.getCurrentPlatform().getLauncherInstallPath();
            if (launcherInstallPath != null) dialog.textFieldInstallPath.setText(launcherInstallPath);
        } else {
            // Use user-provided default, if it was actually provided
            dialog.textFieldInstallPath.setText(defaultInstallPath);
        }

        if (Customization.INSTANCE.getAutomated()) {
            dialog.addWindowListener(new WindowAdapter() {
                @Override
                public void windowOpened(WindowEvent e) {
                    dialog.removeWindowListener(this);
                    dialog.onInstall();
                }
            });
        }

        dialog.pack();
        dialog.setLocationRelativeTo(null);
        dialog.setVisible(true);
        System.exit(0);
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
        System.exit(0);
    }

    private void onBrowse() {
        JFileChooser dirChooser = new JFileChooser();
        dirChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        File currentFile = new File(textFieldInstallPath.getText());
        dirChooser.setSelectedFile(currentFile);
        dirChooser.setCurrentDirectory(currentFile.getParentFile());
        if (dirChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
            textFieldInstallPath.setText(dirChooser.getSelectedFile().getAbsolutePath());
        }
    }

    private void onInstall() {
        if (textFieldPackUrl.getText().trim().isEmpty()) {
            JOptionPane.showMessageDialog(this,
                    strings.getString("error.noPackUrl"),
                    strings.getString("error"),
                    JOptionPane.ERROR_MESSAGE);
            return;
        }
        URL packUrl;
        try {
            packUrl = new URL(textFieldPackUrl.getText());
        } catch (MalformedURLException ex) {
            JOptionPane.showMessageDialog(this,
                    strings.getString("error.invalidPackUrl"),
                    strings.getString("error"),
                    JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (textFieldInstallPath.getText().trim().isEmpty()) {
            JOptionPane.showMessageDialog(this,
                    strings.getString("error.noInstallPath"),
                    strings.getString("error"),
                    JOptionPane.ERROR_MESSAGE);
            return;
        }
        File installDir = new File(textFieldInstallPath.getText());
        if (!installDir.exists()) {
            JOptionPane.showMessageDialog(this,
                    strings.getString("error.invalidInstallPath"),
                    strings.getString("error"),
                    JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (textFieldInstanceName.getText().trim().isEmpty()) {
            JOptionPane.showMessageDialog(this,
                    strings.getString("error.noInstanceName"),
                    strings.getString("error"),
                    JOptionPane.ERROR_MESSAGE);
            return;
        }

        ProgressDialog progressDialog = new ProgressDialog(this);
        Prepper prepper = new Prepper();
        prepper.addProgressCallback((progress, operation) -> SwingUtilities.invokeLater(() -> {
            progressDialog.setText(progress.status);
            progressDialog.setIndeterminate(progress.indeterminate);
            if (!progress.indeterminate) {
                progressDialog.setMaximum(progress.progressMax);
                progressDialog.setMinimum(progress.progressMin);
                progressDialog.setProgress(progress.progressValue);
            }
            if (!progressDialog.isVisible()) progressDialog.setVisible(true);
        }));

        prepper.setInstallDir(installDir);
        prepper.setPackUrl(packUrl);
        prepper.setStrings(strings);
        prepper.setPreferences(textFieldInstanceName.getText(), checkBoxUseIcon.isSelected(), checkBoxUseJoinSetting.isSelected(), checkBoxUseMemorySetting.isSelected());

        prepper.start();
        prepper.getCurrentOperation().handle((ex, result) -> {
            SwingUtilities.invokeLater(() -> {
                progressDialog.setVisible(false);
                progressDialog.dispose();
            });
            return result;
        });
        prepper.getCurrentOperation().exceptionally((ex) -> {
            SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(this,
                    ex.getMessage(),
                    strings.getString("error"),
                    JOptionPane.ERROR_MESSAGE));
            return null;
        });
        prepper.getCurrentOperation().thenAccept((result) -> SwingUtilities.invokeLater(() -> {
            JOptionPane.showMessageDialog(this,
                    strings.getString("installComplete"),
                    strings.getString("installComplete.title"),
                    JOptionPane.INFORMATION_MESSAGE);
            if (Customization.INSTANCE.getDoOutput()) {
                try {
                    HashMap<String, Object> packRoot = new HashMap<>();
                    packRoot.put("packUrl", prepper.getPackUrl());
                    packRoot.put("instancePath", result.getInstanceDir().getAbsolutePath());
                    packRoot.put("instanceName", textFieldInstanceName.getText());
                    packRoot.put("launcherExecutable", result.getLauncherExecutable().getAbsolutePath());
                    System.out.write(new Gson().toJson(packRoot).getBytes(StandardCharsets.UTF_8));
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                dispose();
                System.exit(0);
            }
            })
        );
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        contentPane = new JPanel();
        contentPane.setLayout(new GridBagLayout());
        final JPanel panel1 = new JPanel();
        panel1.setLayout(new GridBagLayout());
        GridBagConstraints gbc;
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.fill = GridBagConstraints.BOTH;
        contentPane.add(panel1, gbc);
        labelPackUrl = new JLabel();
        this.$$$loadLabelText$$$(labelPackUrl, this.$$$getMessageFromBundle$$$("lang/strings", "packUrl"));
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.WEST;
        panel1.add(labelPackUrl, gbc);
        labelInstallPath = new JLabel();
        this.$$$loadLabelText$$$(labelInstallPath, this.$$$getMessageFromBundle$$$("lang/strings", "installPath"));
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.anchor = GridBagConstraints.WEST;
        panel1.add(labelInstallPath, gbc);
        final JPanel panel2 = new JPanel();
        panel2.setLayout(new GridBagLayout());
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 4;
        gbc.fill = GridBagConstraints.BOTH;
        panel1.add(panel2, gbc);
        textFieldInstallPath = new JTextField();
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel2.add(textFieldInstallPath, gbc);
        buttonBrowseInstallPath = new JButton();
        this.$$$loadButtonText$$$(buttonBrowseInstallPath, this.$$$getMessageFromBundle$$$("lang/strings", "browseFolder"));
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel2.add(buttonBrowseInstallPath, gbc);
        textFieldPackUrl = new JTextField();
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel1.add(textFieldPackUrl, gbc);
        checkBoxUseMemorySetting = new JCheckBox();
        this.$$$loadButtonText$$$(checkBoxUseMemorySetting, this.$$$getMessageFromBundle$$$("lang/strings", "useMemorySetting"));
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 8;
        gbc.anchor = GridBagConstraints.WEST;
        panel1.add(checkBoxUseMemorySetting, gbc);
        textFieldInstanceName = new JTextField();
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 7;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel1.add(textFieldInstanceName, gbc);
        labelInstanceName = new JLabel();
        this.$$$loadLabelText$$$(labelInstanceName, this.$$$getMessageFromBundle$$$("lang/strings", "instanceName"));
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 6;
        gbc.anchor = GridBagConstraints.WEST;
        panel1.add(labelInstanceName, gbc);
        checkBoxUseJoinSetting = new JCheckBox();
        this.$$$loadButtonText$$$(checkBoxUseJoinSetting, this.$$$getMessageFromBundle$$$("lang/strings", "useJoinSetting"));
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 9;
        gbc.anchor = GridBagConstraints.WEST;
        panel1.add(checkBoxUseJoinSetting, gbc);
        final JPanel spacer1 = new JPanel();
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 11;
        gbc.fill = GridBagConstraints.VERTICAL;
        panel1.add(spacer1, gbc);
        buttonInstall = new JButton();
        this.$$$loadButtonText$$$(buttonInstall, this.$$$getMessageFromBundle$$$("lang/strings", "startInstall"));
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 12;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel1.add(buttonInstall, gbc);
        checkBoxUseIcon = new JCheckBox();
        this.$$$loadButtonText$$$(checkBoxUseIcon, this.$$$getMessageFromBundle$$$("lang/strings", "useIcon"));
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 10;
        gbc.anchor = GridBagConstraints.WEST;
        panel1.add(checkBoxUseIcon, gbc);
        final JPanel spacer2 = new JPanel();
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        contentPane.add(spacer2, gbc);
        final JPanel spacer3 = new JPanel();
        gbc = new GridBagConstraints();
        gbc.gridx = 2;
        gbc.gridy = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        contentPane.add(spacer3, gbc);
        final JPanel spacer4 = new JPanel();
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.fill = GridBagConstraints.VERTICAL;
        contentPane.add(spacer4, gbc);
        final JPanel spacer5 = new JPanel();
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 2;
        gbc.fill = GridBagConstraints.VERTICAL;
        contentPane.add(spacer5, gbc);
        labelPackUrl.setLabelFor(textFieldPackUrl);
        labelInstallPath.setLabelFor(textFieldInstallPath);
        labelInstanceName.setLabelFor(textFieldInstanceName);
    }

    private static Method $$$cachedGetBundleMethod$$$ = null;

    private String $$$getMessageFromBundle$$$(String path, String key) {
        ResourceBundle bundle;
        try {
            Class<?> thisClass = this.getClass();
            if ($$$cachedGetBundleMethod$$$ == null) {
                Class<?> dynamicBundleClass = thisClass.getClassLoader().loadClass("com.intellij.DynamicBundle");
                $$$cachedGetBundleMethod$$$ = dynamicBundleClass.getMethod("getBundle", String.class, Class.class);
            }
            bundle = (ResourceBundle) $$$cachedGetBundleMethod$$$.invoke(null, path, thisClass);
        } catch (Exception e) {
            bundle = ResourceBundle.getBundle(path);
        }
        return bundle.getString(key);
    }

    /**
     * @noinspection ALL
     */
    private void $$$loadLabelText$$$(JLabel component, String text) {
        StringBuffer result = new StringBuffer();
        boolean haveMnemonic = false;
        char mnemonic = '\0';
        int mnemonicIndex = -1;
        for (int i = 0; i < text.length(); i++) {
            if (text.charAt(i) == '&') {
                i++;
                if (i == text.length()) break;
                if (!haveMnemonic && text.charAt(i) != '&') {
                    haveMnemonic = true;
                    mnemonic = text.charAt(i);
                    mnemonicIndex = result.length();
                }
            }
            result.append(text.charAt(i));
        }
        component.setText(result.toString());
        if (haveMnemonic) {
            component.setDisplayedMnemonic(mnemonic);
            component.setDisplayedMnemonicIndex(mnemonicIndex);
        }
    }

    /**
     * @noinspection ALL
     */
    private void $$$loadButtonText$$$(AbstractButton component, String text) {
        StringBuffer result = new StringBuffer();
        boolean haveMnemonic = false;
        char mnemonic = '\0';
        int mnemonicIndex = -1;
        for (int i = 0; i < text.length(); i++) {
            if (text.charAt(i) == '&') {
                i++;
                if (i == text.length()) break;
                if (!haveMnemonic && text.charAt(i) != '&') {
                    haveMnemonic = true;
                    mnemonic = text.charAt(i);
                    mnemonicIndex = result.length();
                }
            }
            result.append(text.charAt(i));
        }
        component.setText(result.toString());
        if (haveMnemonic) {
            component.setMnemonic(mnemonic);
            component.setDisplayedMnemonicIndex(mnemonicIndex);
        }
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return contentPane;
    }

}
