package com.qlcvea.prepwiz;

import java.io.File;

/**
 * This class contains values that {@link Prepper} identified during execution.
 */
public class PrepperResult {
    /**
     * Path to the executable that starts the launcher.
     */
    final File launcherExecutable;

    /**
     * Path to the directory that contains the generated instance.
     */
    final File instanceDir;

    PrepperResult(File launcherExecutable, File instanceDir) {
        this.launcherExecutable = launcherExecutable;
        this.instanceDir = instanceDir;
    }

    /**
     * Get the path to the executable that starts the launcher.
     */
    public File getLauncherExecutable() {
        return this.launcherExecutable;
    }

    /**
     * Get the path to the directory that contains the generated instance.
     */
    public File getInstanceDir() {
        return this.instanceDir;
    }
}
