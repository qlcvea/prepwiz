package com.qlcvea.prepwiz;

import com.google.gson.Gson;
import com.moandjiezana.toml.Toml;
import com.qlcvea.prepwiz.async.Progress;
import com.qlcvea.prepwiz.async.SingleInstanceProgressAsyncOperation;
import com.qlcvea.prepwiz.platform.Platform;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class Prepper extends SingleInstanceProgressAsyncOperation<PrepperResult> {
    private File installDir = null;
    private String instanceName = null;
    private URL packUrl = null;
    private ResourceBundle strings = null;
    private boolean useIcon = false;
    private boolean useJoinSetting = false;
    private boolean useMemorySetting = false;

    private static final URL bootstrapperUrl;

    static {
        try {
            bootstrapperUrl = new URL("https://github.com/packwiz/packwiz-installer-bootstrap/releases/latest/download/packwiz-installer-bootstrap.jar");
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Converts a version key as described in the <a href="https://packwiz.infra.link/reference/pack-format/pack-toml/#versions">packwiz reference documentation</a> to the UID for the corresponding package in Prism Launcher / PolyMC / MultiMC meta (see <a href="https://meta.prismlauncher.org/v1/index.json">https://meta.prismlauncher.org/v1/index.json</a>)
     * @param versionKey One of the property names of the {@code version} table as described in the <a href="https://packwiz.infra.link/reference/pack-format/pack-toml/#versions">packwiz reference documentation</a>
     * @return The UID for Prism Launcher / PolyMC / MultiMC meta that corresponds to the specified property name
     */
    private static String versionKeyToComponentUid(String versionKey) {
        switch (versionKey) {
            case "minecraft": return "net.minecraft";
            case "fabric": return "net.fabricmc.fabric-loader";
            case "forge": return "net.minecraftforge";
            case "liteloader": return  "com.mumfrey.liteloader";
            case "quilt": return "org.quiltmc.quilt-loader";

            // NeoForge is implemented in the packwiz source code (but not in packwiz-installer right now) despite not appearing in the reference documentation
            // NeoForge is currently only supported on Prism Launcher
            case "neoforge": return "net.neoforged";
            default: throw new IllegalArgumentException("Invalid version key: " + versionKey);
        }
    }

    /**
     * Creates a {@link HashMap} that corresponds to how the specified mmc-pack component should be encoded in mmc-pack.json.
     * @param uid Component UID
     * @param version Component version
     * @return A {@link HashMap} that corresponds to how the specified mmc-pack component should be encoded in mmc-pack.json
     */
    private static HashMap<String, Object> createComponentContent(String uid, String version) {
        HashMap<String, Object> result = new HashMap<>();
        if (uid.equalsIgnoreCase("net.minecraft")) {
            result.put("important", true);
        }
        result.put("uid", uid);
        result.put("version", version);
        return result;
    }

    public void setInstallDir(File installDir) {
        failIfRunning();
        this.installDir = installDir;
    }

    public File getInstallDir() {
        return this.installDir;
    }

    public void setPackUrl(URL packUrl) {
        failIfRunning();
        this.packUrl = packUrl;
    }

    public URL getPackUrl() {
        return this.packUrl;
    }

    public void setStrings(ResourceBundle strings) {
        failIfRunning();
        this.strings = strings;
    }

    public ResourceBundle getStrings() {
        return this.strings;
    }

    public void setPreferences(String instanceName, boolean useIcon, boolean useJoinSetting, boolean useMemorySetting) {
        failIfRunning();
        this.instanceName = instanceName;
        this.useIcon = useIcon;
        this.useJoinSetting = useJoinSetting;
        this.useMemorySetting = useMemorySetting;
    }

    @Override
    protected boolean isReady() {
        return installDir != null && instanceName != null && packUrl != null && strings != null;
    }

    @Override
    protected PrepperResult main() {
        setProgress(new Progress(strings.getString("install.generic")));
        Platform platform = Platform.getCurrentPlatform();
        if (platform == null) throw new IllegalStateException(strings.getString("error.unsupportedPlatform"));

        // check if instance with specified name (safe) already exists
        String instanceNameSafe = platform.filterFilename(instanceName);
        File instanceDir = Paths.get(platform.getInstancesPath(installDir.getAbsolutePath()), instanceNameSafe).toFile();
        if (instanceDir.exists()) {
            throw new RuntimeException(strings.getString("error.instanceDirExists"));
        }

        Properties instanceConfig = new Properties();
        // Create basic instance config values
        instanceConfig.setProperty("InstanceType", "OneSix"); // Only required by MultiMC, without this it would think the instance is using a legacy format
        instanceConfig.setProperty("name", instanceName);
        instanceConfig.setProperty("OverrideCommands", "true");
        instanceConfig.setProperty("PreLaunchCommand", "\"$INST_JAVA\" -jar packwiz-installer-bootstrap.jar " + packUrl.toString() + " --side client");

        // Get the pack.toml file and read its values to find mmc-pack components and optional settings
        HashMap<String, String> componentMap = new HashMap<>();
        setProgress(new Progress(strings.getString("install.retrievingPackMeta")));
        Toml packMeta;
        try {
            URLConnection packMetaConnection = packUrl.openConnection();
            packMetaConnection.addRequestProperty("User-Agent", "prepwiz");
            packMeta = new Toml().read(packMetaConnection.getInputStream());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        if (!packMeta.containsTable("versions")) throw new RuntimeException("pack.toml is invalid - missing versions table");
        for (Map.Entry<String, Object> version : packMeta.getTable("versions").entrySet()) {
            componentMap.put(versionKeyToComponentUid(version.getKey()), version.getValue().toString());
        }
        if (!componentMap.containsKey("net.minecraft")) throw new RuntimeException("pack.toml is invalid - missing Minecraft version");
        if (packMeta.containsTable("prepwiz")) {
            Toml customOptions = packMeta.getTable("prepwiz");

            // server-address
            if (useJoinSetting && customOptions.containsPrimitive("server-address")) {
                instanceConfig.setProperty("JoinServerOnLaunch", "true");
                instanceConfig.setProperty("JoinServerOnLaunchAddress", customOptions.getString("server-address"));
            }

            // memory-max, memory-min (both must be present)
            if (useMemorySetting && customOptions.containsPrimitive("memory-min") && customOptions.containsPrimitive("memory-max")) {
                instanceConfig.setProperty("OverrideMemory", "true");
                instanceConfig.setProperty("MaxMemAlloc", customOptions.getLong("memory-max").toString());
                instanceConfig.setProperty("MinMemAlloc", customOptions.getLong("memory-min").toString());
            }

            // icon
            if (useIcon && customOptions.containsPrimitive("icon")) {
                File iconFile = Paths.get(platform.getIconsPath(installDir.getAbsolutePath()), instanceNameSafe).toFile();
                if (!iconFile.getParentFile().exists() && !iconFile.getParentFile().mkdirs()) {
                    throw new RuntimeException("Failed to create icons directory");
                }
                try {
                    URL iconUrl = new URL(packUrl, customOptions.getString("icon"));
                    URLConnection iconConnection = iconUrl.openConnection();
                    iconConnection.addRequestProperty("User-Agent", "prepwiz");
                    InputStream iconInput = iconConnection.getInputStream();
                    OutputStream iconOutput = Files.newOutputStream(iconFile.toPath());
                    byte[] data = new byte[1024];
                    int byteContent;
                    while ((byteContent = iconInput.read(data, 0, 1024)) != -1) {
                        iconOutput.write(data, 0, byteContent);
                    }
                    iconInput.close();
                    iconOutput.close();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                instanceConfig.setProperty("iconKey", instanceNameSafe);
            }
        }

        // Create the instance folder
        File minecraftDir = Paths.get(instanceDir.getAbsolutePath(), "minecraft").toFile();
        if (!minecraftDir.mkdirs()) {
            throw new RuntimeException("Failed to create Minecraft directory");
        }

        // Download packwiz-installer-bootstrap into minecraftDir
        setProgress(new Progress(strings.getString("install.downloadingInstaller")));
        File bootstrapperFile = Paths.get(minecraftDir.getAbsolutePath(), "packwiz-installer-bootstrap.jar").toFile();
        try (OutputStream bootstrapperOutput = Files.newOutputStream(bootstrapperFile.toPath())) {
            URLConnection bootstrapperConnection = bootstrapperUrl.openConnection();
            bootstrapperConnection.addRequestProperty("User-Agent", "prepwiz");
            InputStream bootstrapperInput = bootstrapperConnection.getInputStream();
            byte[] data = new byte[1024];
            int byteContent;
            int writtenContent = 0;
            while ((byteContent = bootstrapperInput.read(data, 0, 1024)) != -1) {
                bootstrapperOutput.write(data, 0, byteContent);
                writtenContent += byteContent;
                if (bootstrapperConnection.getContentLength() > 0) {
                    setProgress(new Progress(String.format(strings.getString("install.downloadingInstallerPercent"), Math.floorDiv(writtenContent * 100, bootstrapperConnection.getContentLength()))));
                } else {
                    setProgress(new Progress(strings.getString("install.downloadingInstaller")));
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        // Write instance.cfg and mmc-pack.json
        setProgress(new Progress(strings.getString("install.generic")));
        // While "notes" is also a value that must always appear in the instance config,
        // it is set here to have the time correspond to when the instance creation finished
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'"); // ISO 8601
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        instanceConfig.setProperty("notes", String.format(strings.getString("defaultInstanceNotes"), dateFormat.format(new Date())));
        try {
            // instance.cfg
            File instanceConfigFile = Paths.get(instanceDir.getAbsolutePath(), "instance.cfg").toFile();
            //noinspection ResultOfMethodCallIgnored
            instanceConfigFile.createNewFile();
            instanceConfig.store(Files.newOutputStream(instanceConfigFile.toPath()), "");

            // mmc-pack.json
            /*
                { -> HashMap
                    "components": [ -> ArrayList
                        { -> HashMap - items generated from componentMap
                            "important": true // only for net.minecraft
                            "uid": "key of entry in componentMap",
                            "version": "value of entry in componentMap"
                        },
                        ...
                    ],
                    "formatVersion": 1
                }
            */
            File mmcPackFile = Paths.get(instanceDir.getAbsolutePath(), "mmc-pack.json").toFile();
            ArrayList<HashMap<String, Object>> components = new ArrayList<>(componentMap.entrySet().size());
            // Handle net.minecraft separately to ensure it is the first component
            components.add(createComponentContent("net.minecraft", componentMap.get("net.minecraft")));
            for (Map.Entry<String, String> entry : componentMap.entrySet()) {
                if (!entry.getKey().equalsIgnoreCase("net.minecraft"))
                    components.add(createComponentContent(entry.getKey(), entry.getValue()));
            }
            HashMap<String, Object> packRoot = new HashMap<>();
            packRoot.put("components", components);
            packRoot.put("formatVersion", 1);
            //noinspection ResultOfMethodCallIgnored
            mmcPackFile.createNewFile();
            Files.write(mmcPackFile.toPath(), new Gson().toJson(packRoot).getBytes(StandardCharsets.UTF_8), StandardOpenOption.WRITE);

            return new PrepperResult(new File(platform.getLauncherExecutablePath(installDir.getAbsolutePath())), instanceDir);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
