package com.qlcvea.prepwiz.platform.windows;

import com.qlcvea.prepwiz.platform.Platform;
import com.sun.jna.platform.win32.Advapi32Util;
import com.sun.jna.platform.win32.Win32Exception;

import java.io.File;
import java.nio.file.Paths;

import static com.sun.jna.platform.win32.WinReg.HKEY_CURRENT_USER;

public class WindowsPlatform extends Platform {
    public String getDisplayName() {
        return "Windows";
    }

    public boolean isCurrentPlatform() {
        return System.getProperty("os.name").startsWith("Windows");
    }

    public String filterFilename(String filename) {
        String newName = filename.replaceAll("[^\\dA-Za-z \\-_.,]", "_");

        if (newName.endsWith(".")) newName = newName.substring(0, newName.length() - 1);

        int extensionStart = newName.lastIndexOf('.');
        String nameWithoutExtension;
        if (extensionStart >= 0) nameWithoutExtension = newName.substring(0, extensionStart);
        else nameWithoutExtension = newName;

        // forbidden Windows filenames: CON, PRN, AUX, NUL, COM, COMx, LPT, LPTx (where x = a digit)
        if (
            nameWithoutExtension.equalsIgnoreCase("con") ||
            nameWithoutExtension.equalsIgnoreCase("prn") ||
            nameWithoutExtension.equalsIgnoreCase("aux") ||
            nameWithoutExtension.equalsIgnoreCase("nul") ||
            nameWithoutExtension.equalsIgnoreCase("com") ||
            nameWithoutExtension.equalsIgnoreCase("lpt") ||
            (
                nameWithoutExtension.length() == 4 &&
                (nameWithoutExtension.substring(0, 3).equalsIgnoreCase("com") || nameWithoutExtension.substring(0, 3).equalsIgnoreCase("lpt"))
                && Character.isDigit(nameWithoutExtension.codePointAt(3))
            )
        ) {
            newName = "_" + newName;
        }
        return newName;
    }

    public String getLauncherInstallPath() {
        if (!isCurrentPlatform()) return null;
        try {
            return Advapi32Util.registryGetStringValue(HKEY_CURRENT_USER, "Software\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\PrismLauncher", "InstallLocation");
        } catch (Win32Exception ex) {
            // thrown if key does not exist
            // in general this should fail silently as its correct functionality is not critical
            // if this fails then attempt to find polymc
        }
        try {
            return Advapi32Util.registryGetStringValue(HKEY_CURRENT_USER, "Software\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\PolyMC", "InstallLocation");
        } catch (Win32Exception ex) {
            // same as above, except this is the last possibility which means that if this fails then no launcher path could be found
            return null;
        }
    }

    public String getLauncherDataPath(String installPath) {
        if (!isCurrentPlatform()) return null;

        File installDir = new File(installPath);
        if (!installDir.exists()) return null;

        // Default value: launcher data path is the install path
        String appDataPath = installPath;

        if (
                !Paths.get(installPath, "portable.txt").toFile().exists() &&
                !Paths.get(installPath, "MultiMC.exe").toFile().exists()
        ) {
            // PolyMC / Prism behavior: if "portable.txt" exists then data is stored in the same directory as the exe
            // MultiMC behavior: data is always stored in the same directory as the exe
            // therefore if portable.txt does not exist and the launcher is not MultiMC (no exe file named "MultiMC") then app is not in portable mode
            // non-portable mode has the appDataPath as %appdata%\PolyMC or %appdata%\PrismLauncher
            if (Paths.get(installPath, "prismlauncher.exe").toFile().exists()) {
                appDataPath = Paths.get(System.getenv("APPDATA"), "PrismLauncher").toAbsolutePath().toString();
            } else if (Paths.get(installPath, "PolyMC.exe").toFile().exists()) {
                appDataPath = Paths.get(System.getenv("APPDATA"), "PolyMC").toAbsolutePath().toString();
            } else {
                // Could not determine launcher name
                return null;
            }
        }

        return appDataPath;
    }

    public String getLauncherExecutablePath(String installPath) {
        if (!isCurrentPlatform()) return null;

        File installDir = new File(installPath);
        if (!installDir.exists()) return null;

        File exe;
        if (
            (exe = Paths.get(installPath, "MultiMC.exe").toFile()).exists() ||
            (exe = Paths.get(installPath, "prismlauncher.exe").toFile()).exists() ||
            (exe = Paths.get(installPath, "PolyMC.exe").toFile()).exists()
        ) {
            return exe.getPath();
        } else
            return null;
    }
}
