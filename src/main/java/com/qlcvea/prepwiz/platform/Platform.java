package com.qlcvea.prepwiz.platform;

import com.qlcvea.prepwiz.platform.windows.WindowsPlatform;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.Properties;

public abstract class Platform {
    private static final Platform[] platforms = new Platform[] {
            new WindowsPlatform()
    };

    /**
     * Get a {@link Platform} class instance that corresponds to the current platform.
     * @return A {@link Platform} class instance that corresponds to the current platform, or {@code null} if no matching {@link Platform} exists.
     */
    public static Platform getCurrentPlatform() {
        for (Platform platform : platforms) {
            if (platform.isCurrentPlatform()) return platform;
        }
        return null;
    }

    /**
     * Get the display name for this platform.
     * @return Display name.
     */
    public abstract String getDisplayName();

    /**
     * Check if this platform is the one that the application is currently running under.
     * @return {@code true} if this platform is the one that the application is currently running under, {@code false} otherwise.
     */
    public abstract boolean isCurrentPlatform();

    /**
     * Filter a file name to ensure that it is compatible with the current operating system.
     * @param filename The file name to filter.
     * @return The file name, or a slightly altered version to ensure correct functionality.
     */
    public abstract String filterFilename(String filename);

    /**
     * Attempt to find a Prism Launcher / PolyMC installation.
     * @return The Prism Launcher / PolyMC installation path as a {@link String}, or {@code null} if it could not be found.
     */
    public abstract String getLauncherInstallPath();

    /**
     * Find the path to the launcher executable.
     * @param installPath Launcher installation path
     * @return The path as a {@link String}.
     */
    public abstract String getLauncherExecutablePath(String installPath);

    /**
     * Find the path where the launcher stores user data.
     * @param installPath Launcher installation path
     * @return The path as a {@link String}.
     */
    public abstract String getLauncherDataPath(String installPath);

    /**
     * Find a path from Prism Launcher / PolyMC / MultiMC that is defined in the launcher's configuration file.
     * @param installPath Launcher installation path
     * @param defaultName Default name for the path
     * @param configKey The key of the value in the launcher config that stores the path
     * @return The path as a {@link String}.
     */
    public String getLauncherPath(String installPath, String defaultName, String configKey) {
        String appDataPath = getLauncherDataPath(installPath);

        if (appDataPath == null) {
            throw new RuntimeException("Application data path could not be found.");
        }

        // Default value
        String resultPath = defaultName;

        File appData = new File(appDataPath);
        if (appData.exists()) {
            for (File file : Objects.requireNonNull(appData.listFiles())) {
                String name = file.getName();
                if (name.length() > 4 && name.substring(name.length() - 4).equalsIgnoreCase(".cfg")) {
                    // the config file may be named polymc.cfg, prismlauncher.cfg or multimc.cfg
                    // it may not exist if multimc/polymc/prism has never been run
                    Properties props = new Properties();
                    try {
                        InputStream fileInput = Files.newInputStream(file.toPath());
                        props.load(fileInput);
                        fileInput.close();
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                    if (props.containsKey(configKey)) resultPath = props.getProperty(configKey);
                }
            }
        }

        if (Paths.get(resultPath).isAbsolute()) {
            return resultPath;
        } else {
            return Paths.get(appDataPath, resultPath).toAbsolutePath().toString();
        }
    }

    /**
     * Attempt to find the path where Prism Launcher / PolyMC / MultiMC instances are stored from an installation path.
     * @param installPath Launcher installation path
     * @return The path where instances are stored as a {@link String}, or {@code null} if it could not be found.
     */
    public String getInstancesPath(String installPath) {
        return getLauncherPath(installPath, "instances", "InstanceDir");
    }

    /**
     * Attempt to find the path where Prism Launcher / PolyMC / MultiMC icons are stored from an installation path.
     * @param installPath Launcher installation path
     * @return The path where icons are stored as a {@link String}, or {@code null} if it could not be found.
     */
    public String getIconsPath(String installPath) {
        return getLauncherPath(installPath, "icons", "IconsDir");
    }
}
