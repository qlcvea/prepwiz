# prepwiz

A tool to generate MultiMC / PolyMC / Prism Launcher instances for packwiz packs
that are configured to use packwiz-installer.

## Basic usage

1. Java 8 or higher must be installed.  
   [Download Adoptium JRE](https://adoptium.net/temurin/releases?version=8) 
   (choose "JRE" in "Package Type")

2. [Download the latest release](https://gitlab.com/qlcvea/prepwiz/-/releases)
3. Run the JAR file
4. Enter installation settings
   - **Pack URL**: URL to the `pack.toml` file. You should get this from the 
     creator of the modpack.
   - **Installation path**: The path where you have installed/stored Prism
     Launcher, PolyMC or MultiMC.  
     On Windows, this path will be automatically detected if you installed 
     PolyMC or Prism Launcher using their
     [installers](https://prismlauncher.org/download/).
   - **Instance name**: The name you wish to give your instance.
   - **Apply recommended memory settings**, **Automatically connect to 
     server when instance is launched** and **Download icon** will have effect 
     only if the modpack 
     creator has added this information to their `pack.toml` according to 
     the ["Custom `pack.toml` options"](#custom-packtoml-options) section below.

## Customization

prepwiz can be customized to become a modpack-specific installer.

The prebuilt JAR from
[the releases page](https://gitlab.com/qlcvea/prepwiz/-/releases)
contains a `custom.properties` file.  
To edit it, extract the JAR as a ZIP archive, edit the file, then re-zip and 
change the file extension to `.jar` or use an archive application such as 
[7-zip](https://7-zip.org).

This file contains the following properties:

- `title`: Window title
- `packUrl` (commented out by default): URL to the `pack.toml` file.  
  *Setting a pack URL using this property will keep the user from changing it.*
- `defaultInstanceName` (commented out by default): Default name for the 
  instance.
- `defaultUseIcon` (commented out by default): Default value for "Download 
  icon".
- `defaultUseJoinSetting` (commented out by default): Default value for 
  "Automatically connect to server when instance is launched".
- `defaultUseMemorySetting` (commented out by default): Default value for 
  "Apply recommended memory settings".
- `defaultInstallPath` (commented out by default): Default value for "Launcher 
  installation path".
- `automated` (commented out by default): Installation will start automatically
  when the application is opened. This is equivalent to clicking the "Install"
  button as soon as the main window is displayed.  
  *Make sure to set default options for every value!*  
  *The application will stay open after installation if `--output` is not used*

Alternatively, customizations can be applied as command line arguments:

```
usage: java -jar prepwiz.jar
 -a,--automated                            Automatically begin
                                           installation as soon as the
                                           window opens
    --default-install-path <arg>           Default installation path
    --default-instance-name <arg>          Default instance name
 -h,--help                                 Display help
 -o,--output                               Output installation information
                                           when done
    --pack-url <arg>                       Pack URL (not editable if set)
    --title <arg>                          Window title
    --use-icon-default-enabled             Specify this option to enable
                                           the Download icon checkbox by
                                           default
    --use-join-setting-default-enabled     Specify this option to enable
                                           the Automatically connect to
                                           server checkbox by default
    --use-memory-setting-default-enabled   Specify this option to enable
                                           the Apply recommended memory
                                           settings checkbox by default
```

Options passed via command line override options defined in `custom.
properties`, except for the checkbox default values. These are disabled by 
default and will be enabled if defined as such either in `custom.properties` or 
CLI options.

### `--output` option

When this option is enabled installation details will be written to stdout just
before the application terminates if an installation has been completed
successfully.

If this option is enabled the application will terminate after the confirmation
dialog is dismissed when installation is completed.  
Due to the above behavior, it is recommended to use this flag whenever the
`--automated` flag is also used.

Example output:
```json
{
  "packUrl": "https://example.com/mypack/pack.toml",
  "launcherExecutable": "C:\\Users\\username\\PrismLauncher\\prismlauncher.exe",
  "instanceName": "Instance Name",
  "instancePath":"C:\\Users\\username\\PrismLauncher\\instances\\Instance Name"
}
```
*This example has been formatted for clarity. The actual output is minified.*

## Custom `pack.toml` options

prepwiz can read additional options from `pack.toml` to enable additional
features:  
These options should be defined under `[prepwiz]`.

- `icon`: URL to an icon file
- `memory-min`: Recommended value for minimum memory allocation, in MB.
- `memory-max`: Recommended value for maximum memory allocation, in MB.
- `server-address`: Default server address.

If `memory-min` is specified `memory-max` must also be specified (and vice 
versa).

Example file: (based on the
[official example](https://packwiz.infra.link/reference/pack-format/pack-toml/))
```toml
name = "My Modpack"
pack-format = "packwiz:1.0.0"

[index]
file = "index.toml"
hash-format = "sha256"
hash = "e23c098c867dbb45f672cdb407392c7ed1eaa26d21b969ecf64a49d2a937fc0e"

[versions]
forge = "14.23.5.2838"
minecraft = "1.12.2"

[prepwiz]
icon = "icon.png"
memory-min = 512
memory-max = 2048
server-address = "mc.example.com"
```

## Additional notes

- Only supports Windows at this time
- UI cannot be resized properly and is garbage in general
